package com.example.demo.todo.service;

import com.example.demo.todo.model.Todo;
import com.example.demo.todo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoServiceImpl implements TodoService{

    private final TodoRepository todoRepository;

    @Autowired
    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public void createTodo(Todo todo) {
        todoRepository.save(todo);
    }

    @Override
    public void deleteTodo(Long id) {
        todoRepository
                .findById(id)
                .ifPresent(todoRepository::delete);
    }

    @Override
    public Todo getTodo(Long id) {
        return todoRepository.getById(id);
    }

    @Override
    public List<Todo> getAllTodo() {
        return todoRepository.findAll();
    }
}
